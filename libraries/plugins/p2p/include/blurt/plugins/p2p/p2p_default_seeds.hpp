#pragma once

#include <vector>

namespace blurt { namespace plugins { namespace p2p {

#ifdef IS_TEST_NET
const std::vector< std::string > default_seeds;
#else
const std::vector< std::string > default_seeds = {
   "65.21.190.11:1776",    // rpc-1
   "65.21.179.209:1776",   // rpc-2
   "65.21.181.160:1776",   // rpc-3
   "65.21.189.82:1776",    // rpc-4
   "194.163.175.84:1776", // blurtlatam
   "207.244.233.24:1776",  // opfergnome aka nerdtopiade
   "161.97.133.67:1776", // dotwin RPC
   "144.126.142.1:1776", // tekraze RPC
   "blurt-seed1.saboin.com:1776", // saboin
   "blurt-seed2.saboin.com:1776"  // saboin
};
#endif

} } } // blurt::plugins::p2p
